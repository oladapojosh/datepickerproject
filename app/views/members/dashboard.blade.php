@extends('layouts.master')
@include('layouts.header')
@section('content')
    <div class="container">
        <div class="row profile">
            <div class="col-md-3">
                @include('layouts._aside')
            </div>
            <div class="col-md-9">
                    <div class="row main-side">
                        <div class="col-md-12">
                            <div class="panel-heading">
                                <h4 class="colorize">Friend Suggestions<span class="pull-right"><a href="{{ route('dashboard') }}"><i class="fa fa-refresh refresh-style"></i></a></span></h4>
                            </div>
                            <div class="col-md-3">
                                <div class="box">
                                    <div class="img-responsive">
                                        <img src="{{ asset('assets/img/default-avatar.png') }}" width="100%" height="50%"  >
                                    </div>
                                    <div class="panel-body">
                                        <div class="describe">
                                            <div class="ins-name">
                                                <p>James</p>
                                                <em>21, Male</em>
                                            </div>
                                            <div class="panel-footer">
                                                <i class="fa fa-circle color-fa"><span class="style-fa">Available</span></i>
                                                <i class="fa fa-comment color-fa"><span class="style-fa">Message</span></i>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="box">
                                    <div class="img-responsive">
                                        <img src="{{ asset('assets/img/default-avatar.png') }}" width="100%" height="50%"  >
                                    </div>
                                    <div class="panel-body">
                                        <div class="describe">
                                            <div class="ins-name">
                                                <p>James</p>
                                                <em>21, Male</em>
                                            </div>
                                            <div class="panel-footer">
                                                <i class="fa fa-circle color-fa"><span class="style-fa">Available</span></i>
                                                <i class="fa fa-comment color-fa"><span class="style-fa">Message</span></i>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="box">
                                    <div class="img-responsive">
                                        <img src="{{ asset('assets/img/default-avatar.png') }}" width="100%" height="50%"  >
                                    </div>
                                    <div class="panel-body">
                                        <div class="describe">
                                            <div class="ins-name">
                                                <p>Amanda</p>
                                                <em>21, Female</em>
                                            </div>
                                            <div class="panel-footer">
                                                <i class="fa fa-circle color-fa"><span class="style-fa">Not Available</span></i>
                                                <i class="fa fa-comment color-fa"><span class="style-fa">Message</span></i>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="box">
                                    <div class="img-responsive">
                                        <img src="{{ asset('assets/img/default-avatar.png') }}" width="100%" height="50%"  >
                                    </div>
                                    <div class="panel-body">
                                        <div class="describe">
                                            <div class="ins-name">
                                                <p>James</p>
                                                <em>21, Male</em>
                                            </div>
                                            <div class="panel-footer">
                                                <i class="fa fa-circle color-fa"><span class="style-fa">Available</span></i>
                                                <i class="fa fa-comment color-fa"><span class="style-fa">Message</span></i>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
@section('_script')
    <script type="text/javascript">
        var jq = jQuery.noConflict();
        jq(document).ready(function(){
            jq("#search").hide();
            jq("#show-search").click(function(){
                jq("#search").show();
            });
        });
    </script>
@stop
@endsection