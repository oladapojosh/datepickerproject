@extends('layouts.master')

@section('content')
    <section class="all">
        <div class="header">
            <div class="page-header">
                <h1>You are welcome to Member forest <small>| your one stop membership management software</small></h1>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="all-content">
                    <div class="col-md-4"></div>
                    @if(Auth::check())
                        <div class="col-md-4 group-content">
                            <div class="content">
                                <div class="head">
                                    <div class="long-nav">
                                        <nav>
                                            <ul class="nav nav-tabs">
                                                <li role="presentation" class="resize"><a href="{{ route('members.packages') }}">Packages</a></li>
                                                <li role="presentation" class="resize"><a href="{{ route('members.manage') }}">Manage</a></li>
                                                <li role="presentation" class="resize"><a href="{{ route('members.profile') }}">Profile</a></li>
                                                <li role="presentation" class="resize"><a href="{{ route('auth.logout') }}"><button class="btn btn-default btn-sm"><i class="icon-off"></i>Logout</button></a></li>

                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                                <div class="login-form">
                                    <div class="small-message">
                                        <small>Manage your members</small>
                                    </div>

                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="col-md-4"></div>
                </div>
            </div>
        </div>
    </section>
    <div class="foot-content">
        <footer>
            <p>&copy; Copyright publication 2015</p>
        </footer>
    </div>
@endsection