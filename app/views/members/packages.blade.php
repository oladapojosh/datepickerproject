@extends('layouts.master')

@section('content')
    <section class="all">
        <div class="header">
            <div class="page-header">
                <h1>You are welcome to Member forest <small>| your one stop membership management software</small></h1>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="all-content">
                    <div class="col-md-4"></div>
                    @if(Auth::check())
                        <div class="col-md-4 group-content">
                            <div class="content">
                                <div class="head">
                                    <div class="long-nav">
                                        <nav>
                                            <ul class="nav nav-tabs">
                                                <li role="presentation" class="resize"><a href="{{ route('members.packages') }}">Packages</a></li>
                                                <li role="presentation" class="resize"><a href="{{ route('members.manage') }}">Manage</a></li>
                                                <li role="presentation" class="resize"><a href="{{ route('members.profile') }}">Profile</a></li>
                                                <li role="presentation" class="resize"><a href="{{ route('auth.logout') }}"><button class="btn btn-default btn-sm"><i class="icon-off"></i>Logout</button></a></li>

                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                                <div class="login-form">
                                    <div class="small-message">
                                        <small>Member packages</small>
                                    </div>
                                    <div class="package-style">
                                        <div clas="container">
                                            <div class="row">
                                        <div class="col-md-6">
                                            <div class="box">
                                                <div class="title">
                                                    <h3>Trial 7</h3>
                                                </div>
                                                <div class="inner-box">
                                                    <p style=text-align: center>$0 CAD 7days</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="box">
                                                <div class="title">
                                                    <h3>Trial 7</h3>
                                                </div>
                                                <div class="inner-box">
                                                    <p>$0 CAD 7days</p>
                                                </div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @elseif(!Auth::check())
                        <div class="col-md-4 group-content">
                            <div class="content">
                                <div class="head">
                                    <div class="long-nav">
                                        <nav>
                                            <ul class="nav nav-tabs">
                                                <li role="presentation"><a href="{{ route('auth.login') }}">Login</a></li>
                                                <li role="presentation"><a href="{{ route('auth.register') }}">Register</a></li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                                <div class="alert alert-danger"style='text-align: center;' role="alert">
                                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                    <span class="sr-only">Error:</span>
                                   You do not have access to this page, please login or register.
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="col-md-4"></div>
                </div>
            </div>
        </div>
    </section>
    <div class="foot-content">
        <footer>
            <p>&copy; Copyright publication 2015</p>
        </footer>
    </div>

@endsection