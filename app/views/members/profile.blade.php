@extends('layouts.master')
@include('layouts.header')
@section('content')
    <div class="container">
    <div class="row profile">
        <div class="col-md-3">
            @include('layouts._aside')
        </div>
        <div class="col-md-9">
            @include('partials._alert')
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="centralize">Update Profile</h4>
            </div>
            <div class="profile-form">
                <div class="col-md-6">
                {{ Form::open(array('route'=>'profile', 'method'=>'post', 'enctype'=>'multipart/form-data')) }}
                <div class="form-group{{ $errors->has('image-file') ? 'input-error': '' }}">
                    {{ Form::label('image-file', 'Profile Image') }}
                        <span class="btn btn-default btn-lg btn-block btn-file"><span class="pull-right"><i class="color-fa fa fa-folder-open"></i><span class="color-font"> Browse for images...</span></span>{{ Form::file('image-file') }}</span>
                        {{ $errors->profile->first('image-file', '<span class="input-block">:message</span>') }}
                </div>
                    <div class="form-group {{ $errors->has('address') ? 'input-error': '' }}">
                        {{ Form::label('address', 'Home Address') }}
                        {{ Form::text('address',null,  array('class'=>'form-control', 'placeholder'=>'Home Address')) }}
                        {{ $errors->profile->first('address', '<span class="input-block">:message</span>') }}
                    </div>
                    <div class="form-group {{ $errors->has('contact') ? 'input-error': '' }}">
                        {{ Form::label('contact', 'Cell Phone Number') }}
                        {{ Form::text('contact',null,  array('class'=>'form-control', 'placeholder'=>'Phone Number')) }}
                        {{ $errors->profile->first('contact', '<span class="input-block">:message</span>') }}
                    </div>
                    <div class="form-group {{ $errors->has('country') ? 'input-error': '' }}">
                        {{ Form::label('country', 'Country') }}
                        {{ Form::text('country',null,  array('class'=>'form-control', 'placeholder'=>'Country')) }}
                        {{ $errors->profile->first('country', '<span class="input-block">:message</span>') }}
                    </div>
                <div class="form-group {{ $errors->has('age') ? 'input-error': '' }}">
                    {{ Form::label('age', 'Age') }}
                    {{ Form::text('age',null,  array('class'=>'form-control', 'placeholder'=>'Age')) }}
                    {{ $errors->profile->first('age', '<span class="input-block">:message</span>') }}
                </div>


            </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('state') ? 'input-error': '' }}">
                        {{ Form::label('state', 'State') }}
                        {{ Form::text('state',null,  array('class'=>'form-control', 'placeholder'=>'State')) }}
                        {{ $errors->profile->first('state', '<span class="input-block">:message</span>') }}
                    </div>
                    <div class="form-group {{ $errors->has('gender') ? 'input-error': '' }}">
                        {{ Form::label('gender', 'Gender') }}
                        {{ Form::text('gender',null,  array('class'=>'form-control', 'placeholder'=>'Gender')) }}
                        {{ $errors->profile->first('gender', '<span class="input-block">:message</span>') }}
                    </div>
                    <div class="form-group {{ $errors->has('gender') ? 'input-error': '' }}">
                        {{ Form::label('profession', 'What do you do?') }}
                        {{ Form::text('profession',null,  array('class'=>'form-control', 'placeholder'=>'Your profession')) }}
                        {{ $errors->profile->first('profession', '<span class="input-block">:message</span>') }}
                    </div>
                    <div class="form-group {{ $errors->has('age') ? 'input-error': '' }}">
                        {{ Form::label('description', 'Provide a little bit of your description') }}
                        {{ Form::textarea('description',null,  array('class'=>'form-control', 'placeholder'=>'Your description', 'rows'=>'5')) }}
                        {{ $errors->profile->first('age', '<span class="input-block">:message</span>') }}
                    </div>
                </div>
            </div>
            <div class="form-group">
                {{ Form::submit('Update', array('class'=>'btn btn-primary btn-block btn-lg bt-style')) }}
            </div>
            {{ Form::close() }}
        </div>
        </div>
    </div>
    </div>
    @section('_script')
        <script type="text/javascript">
            var jq = jQuery.noConflict();
            jq(document).ready(function(){
                jq("#search").hide();
                jq("#show-search").click(function(){
                    jq("#search").show();
                });
            });
        </script>
    @stop
@endsection