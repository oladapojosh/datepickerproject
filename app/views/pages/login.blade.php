@extends('layouts.master')
@include('layouts.header')
@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-6 col-md-offset-3">
                @include('partials._alert')
                <div class="log-form">
                <h4 class="text-center">Please Login here</h4>

        {{ Form::open(array('route'=>'auth.login')) }}
            <div class="form-group  {{ $errors->login->has('email') ? 'has-error': '' }}">
                {{ $errors->login->first('email', '<span class="help-block">:message</span>') }}
                {{ Form::email('email', null, array('placeholder'=>'Email','class'=>'form-control form-style1')) }}
            </div>
            <div class="form-group  {{ $errors->login->has('password') ? 'has-error': '' }}">
                {{ $errors->login->first('password', '<span class="help-block">:message</span>') }}
                {{ Form::password('password',array('placeholder'=>'Password', 'class'=>'form-control form-style2')) }}
            </div>
        <div class ="checkbox">
            {{ Form::checkbox('remember', null) }} Remember me
        </div>
            {{ Form::submit('Login', array('class'=>'btn btn-default btn-block btn-style')) }}
            {{ Form::close() }}

    </div>
            </div>
        </div>
    </div>
@section('_script')
    <script type="text/javascript">
        var jq = jQuery.noConflict();
        jq(document).ready(function(){
            jq("#search").hide();
            jq("#show-search").click(function(){
                jq("#search").show();
            });
        });
    </script>
@stop
@endsection