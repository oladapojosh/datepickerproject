@extends('layouts.master')
@section('content')
    <div class="head">
        <nav class="nav-style">
            <div class="container">
                <div class="row">
                <div class="title">
                    <p class="fnt_title">datepicker</p>
                </div>
                <div class="nav-menu">
                    <div id="show-log" class="show-login">
                        <span class="sign_in"><a href="{{ route('auth.login') }}"><i class="fa fa-sign-in"></i> Login here</a></span>
                    </div>
                    @if(Auth::check())
                        <div class="show-login">
                        <span>
                       <a href="{{ route('auth.logout') }}">
                           <i class="fa fa-sign-out"></i>
                           Logout
                       </a>
                        </span>
                        </div>
                    @endif
                    @include('partials._alert')

                </div>
            </div>
            </div>
        </nav>
        <div class="container">
        <div class="row">
            <div class="col-md-8 pull-left">
                <h1 class="w_address">Welcome to datepicker</h1>
                <div class="mess_style">
                <small class="message">...we help you build relationships, we stand to bring single men and women together. </small>
                </div>
            </div>
            <div class="col-md-4 pull-right">
                <div class="register-form">
                        <h4 class="reg_style">Register here</h4>

                        {{ Form::open(array('route'=>'auth.register')) }}
                        <div class="form-group {{ $errors->has('firstname') ? 'has-error': '' }}">
                            {{ Form::text('firstname',null, array('class'=>'form-control', 'placeholder'=>'First name')) }}
                            {{ $errors->register->first('firstname', '<span class="help-block">:message</span>') }}
                        </div>
                        <div class="form-group {{ $errors->has('lastname') ? 'has-error': '' }}">
                            {{ Form::text('lastname',null,  array('class'=>'form-control', 'placeholder'=>'Last name')) }}
                            {{ $errors->register->first('lastname', '<span class="help-block">:message</span>') }}
                        </div>
                        <div class="form-group {{ $errors->has('username') ? 'has-error': '' }}">
                            {{ Form::text('username',null,  array('class'=>'form-control', 'placeholder'=>'Username')) }}
                            {{ $errors->register->first('username', '<span class="help-block">:message</span>') }}
                        </div>
                        <div class="form-group {{ $errors->has('email') ? 'has-error': '' }}">
                            {{ Form::text('email',null,  array('class'=>'form-control', 'placeholder'=>'Email')) }}
                            {{ $errors->register->first('email', '<span class="help-block">:message</span>') }}
                        </div>
                        <div class="form-group {{ $errors->has('password') ? 'has-error': '' }}">
                            {{ Form::password('password', array('class'=>'form-control', 'placeholder'=>'Password')) }}
                            {{ $errors->register->first('password', '<span class="help-block">:message</span>') }}
                        </div>
                        <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error': '' }}">
                            {{ Form::password('password_confirmation', array('class'=>'form-control', 'placeholder'=>'Confirm passsword')) }}
                            {{ $errors->register->first('password_confirmation', '<span class="help-block">:message</span>') }}
                        </div>

                        <div class="form-group">
                            {{ Form::submit('Register', array('class'=>'btn btn-primary')) }}
                        </div>
                        {{ Form::close() }}
                </div>
            </div>
        </div>
        </div>
    </div>
    @section('script')
    <script type="text/javascript">
        var jq = jQuery.noConflict();
        jq(document).ready(function(){
            jq("#log-form").hide();
            jq("#show-log").click(function(){
                jq("#log-form").show();
                jq("#show-log").hide();
            });
        });
    </script>
        @stop

@endsection