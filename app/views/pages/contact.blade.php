@extends('layouts.master')

@section('content')

    <section class="all">
        <div class="header">
            <div class="page-header">
                <h1>You are welcome to Member forest <small>| your one stop membership management software</small></h1>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="all-content">
                    <div class="col-md-4"></div>
                    <div class="col-md-4 group-content">
                        <div class="content">
                            <div class="head">
                                <div class="log-nav">
                                    <nav>
                                        <ul class="nav nav-tabs">
                                            <li role="presentation"><a href="{{ route('auth.login') }}">Login</a></li>
                                            <li role="presentation"><a href="{{ route('auth.register') }}">Register</a></li>
                                            <li role="presentation" class="active"><a href="{{ route('pages.contact') }}">Contact us</a></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>

                            <div class="login-form">
                                <div class="small-message">
                                    <small>Please provide your comments, feedbacks,enquiries and questions here. </small>
                                </div>
                                {!! Form::open() !!}
                                <div class="form-group">
                                    {!! Form::text('name', null, ['class'=>'form-control', 'placeholder'=>'Your name']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::text('email',null,  ['class'=>'form-control', 'placeholder'=>'Your email']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::text('subject',null,  ['class'=>'form-control', 'placeholder'=>'Subject of message']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::textarea('message',null,  ['class'=>'form-control', 'placeholder'=>'Your message']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::submit('Submit comments', ['class'=>'btn btn-primary btn-style']) !!}
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                </div>
                    <div class="col-md-4"></div>
            </div>
        </div>
    </section>
    <div class="foot-content">
        <footer>
            <p>&copy; Copyright publication 2015</p>
        </footer>
    </div>
@endsection