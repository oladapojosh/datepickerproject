@extends('layouts.master')
@include('layouts.header')
@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-6 col-md-offset-3">
                @include('partials._alert')
                <div class="log-form">
                    <h4 class="text-center">Please Register here</h4>

                    {{ Form::open(array('route'=>'auth.register')) }}
                    <div class="form-group {{ $errors->has('firstname') ? 'has-error': '' }}">
                        {{ Form::text('firstname',null, array('class'=>'form-control', 'placeholder'=>'First name')) }}
                        {{ $errors->register->first('firstname', '<span class="input-block">:message</span>') }}
                    </div>
                    <div class="form-group {{ $errors->has('lastname') ? 'has-error': '' }}">
                        {{ Form::text('lastname',null,  array('class'=>'form-control', 'placeholder'=>'Last name')) }}
                        {{ $errors->register->first('lastname', '<span class="input-block">:message</span>') }}
                    </div>
                    <div class="form-group {{ $errors->has('username') ? 'has-error': '' }}">
                        {{ Form::text('username',null,  array('class'=>'form-control', 'placeholder'=>'Username')) }}
                        {{ $errors->register->first('username', '<span class="input-block">:message</span>') }}
                    </div>
                    <div class="form-group {{ $errors->has('email') ? 'has-error': '' }}">
                        {{ Form::text('email',null,  array('class'=>'form-control', 'placeholder'=>'Email')) }}
                        {{ $errors->register->first('email', '<span class="input-block">:message</span>') }}
                    </div>
                    <div class="form-group {{ $errors->has('password') ? 'has-error': '' }}">
                        {{ Form::password('password', array('class'=>'form-control', 'placeholder'=>'Password')) }}
                        {{ $errors->register->first('password', '<span class="input-block">:message</span>') }}
                    </div>
                    <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error': '' }}">
                        {{ Form::password('password_confirmation', array('class'=>'form-control', 'placeholder'=>'Confirm passsword')) }}
                        {{ $errors->register->first('password_confirmation', '<span class="input-block">:message</span>') }}
                    </div>

                    <div class="form-group">
                        {{ Form::submit('Register', array('class'=>'btn btn-primary')) }}
                    </div>
                    {{ Form::close() }}

                </div>
            </div>
        </div>
    </div>
@section('_script')
    <script type="text/javascript">
        var jq = jQuery.noConflict();
        jq(document).ready(function(){
            jq("#search").hide();
            jq("#show-search").click(function(){
                jq("#search").show();
            });
        });
    </script>
@stop
@endsection