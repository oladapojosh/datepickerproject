<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}" />
    <meta name="description" content="">
    <title>Date Picker | Getting your dates for you.</title>
    <script src="{{ asset('assets/js/vendor/jquery-1.11.3.js') }}" type="text/javascript"></script>
    <link href="http://fonts.googleapis.com/css?family=Tangerine:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800">
    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />


<body>
<div class="all-content">
@yield('header')

@yield('content')

@yield('script')
@yield('_script')
</div>
</body>
</html>
