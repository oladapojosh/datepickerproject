@section('header')
    <div class="heading">
        <nav class="navbar-style navbar navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a href="{{ route('pages.index') }}" class="navbar-brand">
                        <p class="brand-title">datepicker</p>
                    </a>
                </div>
                <ul class="nav navbar-nav pull-right">
                    <li><a href="{{ route('pages.index') }}"><i class="fa fa-home" title="home"></i></a></li>
                    <li><a href="{{ route('auth.login') }}"><i class="fa fa-sign-in" title="login"></i></a></li>
                    <li><a href="{{ route('auth.register') }}"><i class="fa fa-user-plus" title="register"></i></a></li>
                    <li id="show-search"><a href="#"><i class="fa fa-search" title="search"></i></a></li>
                    <li id="search"><input type="search" placeholder="search" class="form-control"></li>
                    @if(Auth::check())
                    <li><a href="{{route('auth.logout') }}"><i class="fa fa-sign-out" title="logout"></i></a> </li>
                        <li><a href="{{route('dashboard') }}"><i class="fa fa-dashboard" title="dashboard"></i></a> </li>
                    @endif

                </ul>
            </div>
        </nav>
    </div>
    @stop