<aside class="profile-sidebar">
    <div class="profile-pic">
        <img src="{{ asset('assets/img/default-avatar.png') }}">
    </div>
    <div class="profile-title">
        <p class="g_title">Hello,  </p>
    </div>
    <div class="profile-usermenu">
        <ul class="nav a_style">
            <li class ="{{ (Request::url() == route('dashboard')) ? 'active': ''}}" >
                <a href="{{ route('dashboard') }}">
                    <i class="fa fa-home"></i>
                    Overview </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-comments"></i>
                    Chat </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-smile-o"></i>
                    Friends </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-heart-o"></i>
                    Profile Likes </a>
            </li>
            <li class ="{{ (Request::url() == route('profile')) ? 'active': ''}}">
                <a href="{{ route('profile') }}">
                    <i class="fa fa-user"></i>
                    Account settings</a>
            </li>

            <li>
                <a href="#">
                    <i class="fa fa-flag-o"></i>
                    Help </a>
            </li>
        </ul>
    </div>
</aside>