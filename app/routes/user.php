<?php
Route::group(['prefix'=>'members'], function(){

    Route::match(['GET'],'profile', [
        'as'=>'profile',
        'uses'=>'ProfileController@createProfile'
    ]);
    Route::post('profile', [
        'as'=>'profile',
        'uses'=>'ProfileController@saveProfile'
    ]);

    Route::match(['GET'],'dashboard', [
        'as'=>'dashboard',
        'uses'=>'MemberController@getDashboard'
    ]);

});
