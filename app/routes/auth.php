<?php
Route::get('/logout', [
    'as'=>'auth.logout',
    'uses'=>'AuthController@getLogout'
]);

Route::group(['prefix'=>'auth'], function(){

    Route::match(['GET', 'POST'], 'login', [
       'as'=> 'auth.login',
        'uses'=>'AuthController@login'
    ]);

    Route::match(['GET'], 'register', [
        'as'=> 'auth.register',
        'uses'=>'AuthController@getRegistrationForm'
    ]);
    Route::post('register', [
        'as'=> 'auth.register',
        'uses'=>'AuthController@postRegistrationForm'
    ]);

});

