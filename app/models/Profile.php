<?php
class Profile extends Eloquent {

    protected $fillable =[
        'image',
        'gender',
        'address',
        'contact',
        'state',
        'country',
        'age',
        'description',
        'profession'
    ];
    protected $table = 'profiles';

    public function user(){
        $this->belongsTo('User');
    }
}