<?php
ini_set('memory_limit', '500M');
class ProfileController extends \BaseController {

	public function getProfile(){

	}
    public function createProfile(){
        return View::make('members.profile');
    }
    public function saveProfile(){
        $input = [
            'image'=>Input::file('image-file'),
            'gender'=>Input::get('gender'),
            'address'=>Input::get('address'),
            'contact'=>Input::get('contact'),
            'state'=>Input::get('state'),
            'country'=>Input::get('country'),
            'age'=>Input::get('age'),
            'description'=>Input::get('description'),
            'profession'=>Input::get('profession')
        ];

        $validate = [
            'image-file'=> 'image',
            'gender' => 'required',
            'address' => 'required',
            'contact' => 'required',
            'state' => 'required',
            'country' => 'required',
            'age' => 'required',
            'description'=>'required|max:100|',
            'profession'=>'required'
        ];

        $validator = Validator::make($input, $validate);
        if($validator->fails()){
            return Redirect::route('profile')->withErrors($validator, 'profile')->withInput();
        }
        if($validator->passes()){
                if(Input::hasfile('image-file')){
                    $random_key = sha1(time(). microtime());
                    $extension = Input::file('image-file')->getClientOriginalExtension();
                    $filename = $random_key . '.'. $extension;
            $destination_path =  public_path(). '/assets/uploads/';
            if (!File::isDirectory($destination_path)) {
                File::makeDirectory($destination_path, 0777);
            }
            $upload_success = Input::file('image-file')->move($destination_path, $filename);
            if($upload_success) {
                $profile_photo = $filename;
            }
            $user = new User();
            $profile = new Profile();
            $profile->image = $profile_photo;
            $profile->gender = $input['gender'];
            $profile->address = $input['address'];
            $profile->contact = $input['contact'];
            $profile->state = $input['state'];
            $profile->country = $input['country'];
            $profile->age = $input['age'];
            $profile->description = $input['description'];
            $profile->profession = $input['profession'];
            $success = $user->profile()->save($profile);
                    if ($success) {
                        return Redirect::route('profile')->with('message', 'Profile Update successful');
                    } else {
                        return Redirect::route('profile')->withErrors('An error occured while updating profile')->withInput();
                    }
                }else{
                    return Redirect::route('profile')->withErrors('Image Upload error!')->withInput();
                }
        }
    }
    public function updateProfile(){

    }
    public function deleteProfile(){

    }

}
