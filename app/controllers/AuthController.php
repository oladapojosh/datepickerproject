<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class AuthController extends BaseController
{
    public function getRegistrationForm()
    {
        return View::make('pages.register');
    }
    public function postRegistrationForm()
    {
        $input = [
            'firstname' => Input::get('firstname'),
            'lastname' => Input::get('lastname'),
            'username' => Input::get('username'),
            'email' => Input::get('email'),
            'password' => Input::get('password'),
            'password_confirmation' => Input::get('password_confirmation')
        ];
        $rules = [
            'firstname' => 'required',
            'lastname' => 'required',
            'username' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:8',
            'password_confirmation' => 'required|min:8'
        ];
        $validator = Validator::make($input, $rules);
        if ($validator->passes()) {
            $user = User::create([
                'firstname'=>$input['firstname'],
                'lastname'=>$input['lastname'],
                'username'=>$input['username'],
                'email'=>$input['email'],
                'password'=>Hash::make($input['password']),
            ]);
            if ($user) {
                Auth::login($user);
                return Redirect::route('dashboard')->withSuccess('You are signed in, Please complete your profile');
            }
        } else {
            return Redirect::route('auth.register')->withErrors($validator, 'register')->withInput();
        }
    }

    public function login(){
        if(Request::isMethod('post')){
            $data = Input::only('email', 'password');
            $rule = [
                'email'=> 'email|required',
                'password'=> 'required'
            ];
            $validate = Validator::make($data, $rule);
            if($validate->fails()){
                return Redirect::route('auth.login')->withErrors($validate, 'login')->withInput();
            }
            if($validate->passes()){
                $attempt = Auth::attempt(array($data['email'],$data['password']));
                    if($attempt){
                        return Redirect::route('update')->with('message', "You are welcome");
                    }else{
                        return Redirect::route('auth.login')->withErorrs("An error has occurred! ");
                    }
            }
        }
        return View::make('pages.login');
    }
    public function getLogout(){
        Auth::logout();
        return Redirect::route('pages.index')->withSuccess('You have logged out successfully');
    }
}
